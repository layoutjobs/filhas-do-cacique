<!-- 
    adicionar rolagem ao botao apoe
    ultimo botao mudar texto e linkar para o catarse

    apoie
    catarse.me
 -->


<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=no'>
    

    <title>Filhas do Cacique</title>

    <!-- Base Link -->
    <!-- <base href="/"> -->
    <!-- <base href="/filhas/"> -->

    <!-- Favicon -->
    <link rel="icon" href="assets/img/favicon.ico">

    <!-- Css -->
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/fonts.css">

    <!-- HTML5 Shiv -->
    <!--[if lt IE 9]><script src="assets/js/vendor/html5.js"></script><![endif]-->
</head>

<body>
    <header>
        <nav class="hidden-sm hidden-xs">
            <span class="container">
                <a data-section="#section1">O JOGO</a>
                <a data-section="#section2">VÍDEOS</a>
                <a data-section="#section3">SEJA UM APOIADOR</a>
                
                <span class="languages">
                    <a href="#versao-portugues" title="Versão Português"></a>
                    <a href="en/" title="English Version"></a>
                </span>
            </span>
        </nav>

        <span class="languages visible-sm visible-xs" style="top: 15px; right: 15px">
            <a href="#versao-portugues" title="Versão Português"></a>
            <a href="en/" title="English Version"></a>
        </span>
    </header>

    <div class="about" id="section1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="call">&nbsp;</p>
                    <p class="title">VOCÊ TEM CORAGEM DE ENCARAR O DESAFIO?</p>
                </div>
            </div>

            <div class="row">
                <div class="how-to col-md-6 col-sm-6">
                    <p class="title arrow">Como funciona:</p>
                    <p class="description">Construa suas ferramentas a partir de cartas de matéria prima.
                    Caminhe pela floresta movendo-se por um tabuleiro construído de forma diferente em cada partida.
                    Colete flora, fauna e confronte lendas, comprando cartas do topo de diferentes baralhos.
                    Utilize as cartas para aprimorar seu índio, anexando em suas folhas de atributos.
                    Para então impressionar o Grande Cacique que só fara a escolha do Novo Cacique no final do jogo.</p>

                    <img class="img-responsive hidden-xs" src="assets/img/01.jpg">
                </div>
                <div class="col-md-6 col-sm-6">
                    <img class="img-responsive" src="assets/img/02.png">
                    <a class="button" class="hidden-xs">SEJA UM APOIADOR</a>
                </div>
            </div>
        </div>
    </div>

    <div class="banner">
        <div class="container">
            <div class="row">

                <div class="img col-md-7 col-sm-6 col-xs-12">
                    <img src="assets/img/box-saci.png" alt="" class="img-responsive center-block">
                </div>

                <div class="text title col-md-5 col-sm-6 col-xs-12">
                    <p>QUEM ACEITARÁ O DESAFIO DO CACIQUE?</p>
                </div>

                <div class="text col-md-5 col-sm-6 col-xs-12">
                    <p>Idealizado e projetado no Brasil por brasileiros.
                        Para quem gosta de passar bons momentos junto a amigos e familiares.
                        Original, criativo, intuitivo e imprevisível.
                        Apoie nosso projeto e garanta seu exemplar.
                    </p>
                    <p class="bold">Leia abaixo para saber como adquirir o seu game.</p>
                </div>
                
            </div>
        </div>
    </div>

    <div class="videos" id="section2">
        <div class="container small">
            
            <div class="row">
                <div class="col-md-12">
                    <p class="main title">VÍDEOS E <span class="visible-xs"></span> GAMEPLAY
                        <span class="call hidden-xs">
                            Assista aos vídeos e curta um pouco mais do card game As Filhas do Cacique.
                            <br>Saiba como comprar no final da página.
                        </span>
                    </p>
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/DlfsSLB7KT4" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            
        </div>

        <?php /*
        <div class="container">
            
            <div class="row">
                <div class="thumb col-md-4 col-sm-4 col-xs-4" data-video="https://www.youtube.com/embed/xGqns87AfcE">
                    <span class="glyphicon glyphicon-play-circle"></span>
                    <span>Teaser</span>
                </div>

                
                <div class="thumb col-md-4 col-sm-4 col-xs-4" data-video="https://www.youtube.com/embed/caLLrBBrBOM">
                    <span class="glyphicon glyphicon-play-circle"></span>
                    <span>Setup</span>
                </div>

                <div class="thumb col-md-4 col-sm-4 col-xs-4" data-video="https://www.youtube.com/embed/oNkx6FlwEK0">
                    <span class="glyphicon glyphicon-play-circle"></span>
                    <span>Game</span>
                </div>
            </div>
            
        </div>
        */ ?>
    </div>

    <div class="supporter" id="section3">
        <div class="container">
            
            <div class="row">

                <div class="col-md-1 hidden-sm hidden-xs"></div>

                <div class="col-md-8 col-sm-12">

                    <div class="row">
                        <div class="col-md-12">
                            
                            <span class="title">Seja um apoiador</span>

                            <img class="logo-catarse img-responsive" src="assets/img/logo-catarse.png" alt="">
                            
                            <p>Para que você possa ter um jogo As Filhas do Cacique apoie nosso projeto no Catarse.</p>
                            <p>Ajude a transformar idéias em realidade. Acesse o link abaixo e saiba mais como funciona.</p>

                        </div>
                    </div>

                    <div class="row">
                        
                        <div class="col-md-7 hidden-sm hidden-xs">
                            <img class="box-sapo img-responsive" src="assets/img/box-sapo.png" alt="">
                        </div>
                        
                        <div class="text col-md-5">
                            <!-- <a class="button" href="https://www.catarse.me/pt/cacique">APOIE</a> -->
                            <a class="button" style="background: #ccc; color: inherit; cursor: default; text-decoration: none;">APOIE<br>(EM BREVE)</a>
                            
                            <ul class="hidden-sm hidden-xs">
                                <li class="header">Apoiadores</li>
                                <li>Humberto Jacques de Medeiros</li>
                                <li>Carlos Vinícius Lorençon</li>
                                <li>Diogo Abreu Meyer Pires</li>
                                <li>Ricardo Bonato</li>
                                <li>Jackson Luiz De Marco</li>
                                <li>Wagner de Almeida Santos</li>
                                <li>Luis Guilherme Bonafé Gaspar Ruas</li>
                                <li>Katiani Martins de Oliveira</li>
                                <!-- <li>Douglas Emanoel Mendes de Oliveira</li> -->
                            </ul>
                        </div>
                    </div>

                </div>


                <div class="col-md-2 col-sm-12">
                    <img class="img-responsive col-md-12 col-sm-3 col-xs-4" src="assets/img/selo-catarse-pt.png" alt="" style="margin: 40px auto">
                    <div class="col-sm-1"></div>
                    <img class="img-responsive col-md-12 col-sm-4 col-xs-4" src="assets/img/selo-fdc.png" alt="" style="margin: 40px auto">
                    <div class="col-sm-1"></div>

                    <a href="http://fb.com/asfilhasdocacique">
                        <img class="img-responsive col-md-12 col-sm-3 col-xs-4" src="assets/img/fb.png" alt="" style="margin: 40px auto">
                    </a>
                </div>

                <div class="col-md-1 hidden-sm hidden-xs"></div>

            </div>


            
        </div>
    </div>

    <div class="form">
        <div class="container">
            <div class="row">
                <div class="col-md-1 hidden-sm hidden-xs"></div>

                <div class="col-md-10">

                    <p class="title arrow">PARA DÚVIDAS, SUGESTÕES OU CRÍTICAS, 
                        <br>FALE COM O CACIQUE:</p>
                    
                    <form method="post" action="inc/send-contact.php" onsubmit="teste(this)">
                        <label for="">Nome</label>
                        <input name="name" type="text" >

                        <label for="">Email</label>
                        <input name="email" type="email" >

                        <label for="">Mensagem</label>
                        <textarea name="msg" id="" cols="30" rows="10"></textarea>

                        <input class="submit" type="submit">
                    </form>
                    
                </div>

                <div class="col-md-1 hidden-sm hidden-xs"></div>
                
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <img class="img-responsive" src="assets/img/logo-monster-games.png" alt="">                    
            </div>
        </div>
    </footer>
  

    <!-- Js -->
    <script src="assets/js/vendor/jquery.js"></script>
    <script src="assets/js/vendor/bootstrap.js"></script>
    <script src="assets/js/lib/app.js"></script>

    <!-- Analytics -->
    <script src="assets/js/vendor/analytics.js"></script>
    <script src="assets/js/scroll.js"></script>

    <script>
        $(document).ready(function () {
            $('.videos .thumb').on('click', function(){
                video = $(this).attr('data-video')
                title = $(this).find('p').html()

                $('iframe').attr('src', video)
                // $('.video-title').html(title)
            })
        })

        var x = $("iframe").parent().width();
        x = x * 0.56
        $("iframe").attr('height', x)
    </script>

</body>
</html>



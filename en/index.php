<!doctype html>
<html lang="pt-BR">
<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=no'>
    

    <title>Chieftain's Daughters</title>

    <!-- Base Link -->
    <!-- <base href="/en/"> -->
    <!-- <base href="/filhas/en/"> -->

    <!-- Favicon -->
    <link rel="icon" href="assets/img/favicon.ico">

    <!-- Css -->
    <link rel="stylesheet" href="../assets/css/bootstrap.css">
    <link rel="stylesheet" href="../assets/css/main.css">
    <link rel="stylesheet" href="../assets/css/fonts.css">

    <!-- HTML5 Shiv -->
    <!--[if lt IE 9]><script src="../assets/js/vendor/html5.js"></script><![endif]-->

</head>
<body>
    <header>
        <nav class="hidden-sm hidden-xs">
            <span class="container">
                <a data-section="#section1">THE GAME</a>
                <a data-section="#section2">VIDEOS</a>
                <a data-section="#section3">BE A SUPPORTER</a>
                
                <span class="languages">
                    <a href="../" title="Versão Português"></a>
                    <a href="#english-version" title="English Version"></a>
                </span>
            </span>
        </nav>

        <span class="languages visible-sm visible-xs" style="top: 15px; right: 15px">
            <a href="../" title="Versão Português"></a>
            <a href="#english-version" title="English Version"></a>
        </span>

    </header>

    <div class="about" id="section1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="call">The great Chief wants to marry his two daughters:
                        <br>Beauty and Not So Beautiful.</p>
                    <p class="title">YOU HAVE COURAGE TO FACE THE CHALLENGE?</p>
                </div>
            </div>

            <div class="row">
                <div class="how-to col-md-6 col-sm-6">
                    <p class="title arrow">How it works:</p>
                    <p class="description">Assembly your tools with the raw materialcards. 
                    Walk through the forest, moving on across a changing board in each race. 
                    Gathering flora, fauna and facing legends. Draw cards from each deck. 
                    Use the gathered cards to increase your indian character attributes; attaching these cards at the sleeve.
                    And in the end of the game. For then impress the Great Cacique that only make the choice of New Cacique at the end of the game.</p>
                    <img class="img-responsive hidden-xs" src="../assets/img/01.jpg">
                </div>
                <div class="col-md-6 col-sm-6">
                    <img class="img-responsive" src="../assets/img/02.png">
                    <a class="button" class="hidden-xs">BE A SUPPORTER</a>
                </div>
            </div>
        </div>
    </div>

    <div class="banner">
        <div class="container">
            <div class="row">

                <div class="img col-md-7 col-sm-6 col-xs-12">
                    <img src="../assets/img/box-saci.png" alt="" class="img-responsive center-block">
                </div>



                <div class="text title col-md-4 col-sm-6 col-xs-12">
                    <p>WHO ACCEPT THE CHIEFTAIN CHALLENGE?</p>

                </div>

                <div class="text col-md-4 col-sm-6 col-xs-12">
                    <p>Conceived and designed by Brazilian in Brazil; 
                    this is for who wants to have good time with relatives and buddies. 
                    Original, Creative, Unpredictable, Groundbreaking.</p>
                    <p class="bold">Support our project and ensure you own copy.</p>
                </div>

                
            </div>
        </div>
    </div>

    <div class="videos" id="section2">
        <div class="container small">
            
            <div class="row">
                <div class="col-md-12">
                    <p class="main title">VIDEOS AND GAMEPLAY
                        <!-- <span class="call hidden-xs">
                            Assisti a um game play e curta um pouco mais do card game As Filhas do Cacique.
                            <br>Saiba como comprar no final da página.
                        </span> -->
                    </p>
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/DlfsSLB7KT4" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            
        </div>

        <?php /*
        <div class="container">

            <div class="row">
                <div class="thumb col-md-4 col-sm-4 col-xs-4" data-video="https://www.youtube.com/embed/xGqns87AfcE">
                    <span class="glyphicon glyphicon-play-circle"></span>
                    <span>Teaser</span>
                </div>

                <div class="thumb col-md-4 col-sm-4 col-xs-4" data-video="https://www.youtube.com/embed/caLLrBBrBOM">
                    <span class="glyphicon glyphicon-play-circle"></span>
                    <span>Setup</span>
                </div>

                <div class="thumb col-md-4 col-sm-4 col-xs-4" data-video="https://www.youtube.com/embed/oNkx6FlwEK0">
                    <span class="glyphicon glyphicon-play-circle"></span>
                    <span>Game</span>
                </div>
            </div>
            
        </div>
        */ ?>
    </div>

    <div class="supporter" id="section3">
        <div class="container">
            
            <div class="row">

                <div class="col-md-1 hidden-sm hidden-xs"></div>

                <div class="col-md-8 col-sm-12">

                    <div class="row">
                        <div class="col-md-12">
                            
                            <span class="title">BE A SUPPORTER</span>

                            <img class="logo-catarse img-responsive" src="../assets/img/logo-catarse.png" alt="">
                            
                            <p>To purchase a game, "The Daughters of the Cacique" support our project.</p>
                            <p>Help turn this idea into reality. Follow the link below to learn better how it works.</p>

                        </div>
                    </div>

                    <div class="row">
                        
                        <div class="col-md-5 hidden-sm hidden-xs">
                            <img class="box-sapo img-responsive" src="../assets/img/box-sapo.png" alt="">
                        </div>
                        
                        <div class="text col-md-7">
                            <!-- <a class="button" href="https://www.catarse.me/pt/cacique">SUPPORT</a> -->
                            <a class="button" style="background: #ccc; color: inherit; cursor: default; text-decoration: none;">SUPPORT<br>(SOON)</a>
                            
                            <ul class="hidden-sm hidden-xs">
                                <li class="header">Supporters</li>
                                <li>Carlos Vinícius Lorençon</li>
                                <li>Katiani Martins de Oliveira</li>
                                <li>Douglas Emanoel Mendes de Oliveira</li>
                                <li>Diogo Abreu Meyer Pires</li>
                            </ul>
                        </div>
                    </div>


                    
                </div>


                <div class="col-md-2 col-sm-12">
                    <img class="img-responsive col-md-12 col-sm-3 col-xs-4" src="../assets/img/selo-catarse-pt.png" alt="" style="margin: 40px auto">
                    <div class="col-sm-1"></div>
                    <img class="img-responsive col-md-12 col-sm-4 col-xs-4" src="../assets/img/selo-fdc.png" alt="" style="margin: 40px auto">
                    <div class="col-sm-1"></div>
                    <img class="img-responsive col-md-12 col-sm-3 col-xs-4" src="../assets/img/fb.png" alt="" style="margin: 40px auto">
                </div>

                <div class="col-md-1 hidden-sm hidden-xs"></div>

            </div>


            
        </div>
    </div>

    <div class="form">
        <div class="container">
            <div class="row">
                <div class="col-md-1 hidden-sm hidden-xs"></div>

                <div class="col-md-10">

                    <p class="title arrow">
                        FOR QUESTIONS, SUGGESTIONS OR REVIEWS, 
                        <br>TALK TO THE CHIEFTAIN:</p>
                    
                    <form method="post" action="inc/send-contact.php" onsubmit="teste(this)">
                        <label for="">Name</label>
                        <input name="name" type="text" >

                        <label for="">Email</label>
                        <input name="email" type="email" >

                        <label for="">Message</label>
                        <textarea name="msg" id="" cols="30" rows="10"></textarea>

                        <input class="submit" type="submit" value="Send">
                    </form>
                    
                </div>

                <div class="col-md-1 hidden-sm hidden-xs"></div>
                
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <img class="img-responsive" src="../assets/img/logo-monster-games.png" alt="">                    
            </div>
        </div>
    </footer>
  

    <!-- Js -->
    <script src="../assets/js/vendor/jquery.js"></script>
    <script src="../assets/js/vendor/bootstrap.js"></script>
    <script src="../assets/js/lib/app.js"></script>

    <!-- Analytics -->
    <script src="../assets/js/vendor/analytics.js"></script>
    <script src="../assets/js/scroll.js"></script>

    <script>
        $(document).ready(function () {
            $('.videos .thumb').on('click', function(){
                video = $(this).attr('data-video')
                title = $(this).find('p').html()

                $('iframe').attr('src', video)
            })
        })

        var x = $("iframe").parent().width();
        x = x * 0.56
        $("iframe").attr('height', x)
    </script>

</body>
</html>
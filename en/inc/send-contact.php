
<?php
    extract($_POST);

    date_default_timezone_set("Brazil/East");

    require_once 'PHPMailer/PHPMailerAutoload.php';

    $to = array('wscesar@gmail.com', 'william@layoutnet.com.br');

    $Subject = strtoupper($name).' enviou uma mensagem pelo site Filhas do Cacique';

    $Message = '
        <table>
            <tr>
                <td><b>'.strtoupper($name).'</b>, enviou um pedido através do site Filhas do Cacique.</td>
            </tr>

            <tr>
                <td>'.$email.'</td>
            </tr>

            <tr>
                <td>'.$msg.'</td>
            </tr>

            <tr>
                <td><br><font face="Arial" color="#505050">Email enviado em: '.date('d/m/Y - H:i').'</font></td>
            </tr>
        </table>';

    $Message = utf8_decode($Message);

    $Host = 'smtp.layoutnet.com.br';
    $Username = 'william@layoutnet.com.br';
    $Password = 'fDxC23zg';
    $Port = "587";

    $mail = new PHPMailer();
    $body = $Message;
    $mail->IsSMTP();

    $mail->Host = $Host;
    $mail->SMTPDebug = 0;
    $mail->SMTPAuth = true;
    $mail->Port = $Port;
    $mail->Username = $Username;
    $mail->Password = $Password;

    $mail->SetFrom($Username, 'Filhas do Cacique');
    $mail->Subject = $Subject;
    $mail->MsgHTML($body);
    
    foreach($to as $to_add) {
        $mail->AddAddress($to_add);
    }

    if(!$mail->Send()) {
        echo  'Erro ao enviar e-mail: '. print($mail->ErrorInfo);
    } else {
        echo '<script>window.location="../obrigado"</script>';
    }
?>